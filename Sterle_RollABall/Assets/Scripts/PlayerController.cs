﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public Text countText;
    public Text winText;

    private Rigidbody rb;
    private int count;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }

        // Allows the booster pads to quickly increase the speed of the player for a sho after running over the boost pad.
        if (other.gameObject.CompareTag("Booster"))
        {
            rb.velocity *= 4.5f;
        }


    }
    // When the player hits the hole after flying off the ramp, the count text on the top left of the screen is set to empty, and a big message in the center of the screen announces a win and
    // number of pickups collected.
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.other.gameObject.CompareTag("Hole"))
        {
            winText.text = "You Win! Pick ups collected: " + count.ToString();
            countText.text = "";
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
    }
}